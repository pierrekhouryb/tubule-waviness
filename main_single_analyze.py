import os
import sys
import numpy
import lib.ioutils as ioutils
import lib.detect_peaks
import imageutils
import plotutils
import imageutils_tests
import scipy
import scipy.stats as stats
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import math
from sklearn import mixture

#Make sure we have the right inputs
if len(sys.argv) != 2:
	print('Usages are: python main.py arg1 arg2')
	sys.exit()

#Start
print('Running %s' % sys.argv[0])
fin = sys.argv[1]

print('input: %s' % fin)

datadir = 'images/'

processedfilename = datadir + 'processed/' + ioutils.getfilenamewithoutextension(fin)

orientation = numpy.reshape(ioutils.loadf8le(processedfilename + '_orient'),(512, 512))
energy = numpy.reshape(ioutils.loadf8le(processedfilename + '_energy'),(512, 512))
coherency = numpy.reshape(ioutils.loadf8le(processedfilename + '_cohere'),(512, 512))
# plotutils.plotImage(orientation, 'orientation')

energyMask = imageutils.createMaskBasedOnEnergy(energy, 0.02)
mask = energyMask * coherency

maskedOrientation = (orientation * mask)

# imageutils.computeMainOrientations(orientation, energyMask * coherency)
bins = 256

h, binEdges = numpy.histogram(orientation.ravel(), bins=bins, \
                                                   range=(numpy.min(orientation), numpy.max(orientation)), \
                                                   weights=mask.ravel())
bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
xmin = binEdges.min()
xmax = binEdges.max()
print('Resolution: ' + str(180*(xmax-xmin)/(bins*math.pi)))
x = numpy.linspace(xmin,xmax,bins)
smooth_h = imageutils.smoothdatacurve(h,window_size=21,order = 2)
fig, ax = plt.subplots()
ax.plot(x, h, 'k')
ax.plot(x, imageutils.smoothdatacurve(h),'b')
ax.plot(x, smooth_h,'r')

peaks = lib.detect_peaks.detect_peaks(smooth_h, mph=0.6*max(smooth_h), mpd=10, show=False)
peaks = bincenters[peaks]

hcurve = imageutils.histogramtocurve(smooth_h,bincenters)
# fig, ax = plt.subplots()
# ax.plot(hcurve)

# samples = numpy.transpose(numpy.vstack((bincenters, h)))
# samples = h.reshape(h.size, 1)
# samples = (orientation * mask).ravel().reshape(orientation.size, 1)
# samples = orientation.ravel().reshape(orientation.size, 1)

#####
samples = hcurve #maskedOrientation.ravel().reshape(orientation.size, 1)
print samples.shape

#init gmm
#refactor and try to fit individual gaussians based on peaks and valleys
n_components = len(peaks)

g = mixture.GMM(n_components=n_components, covariance_type='full', min_covar=1e-3, params='wmc')
g.means_ = peaks
g.fit(samples)

weights = g.weights_
means = g.means_
covars = g.covars_

print weights
print means
print covars

color = ['red','green','blue','yellow','cyan']

fig, ax = plt.subplots()
for k in xrange(means.size):
	mean = means.ravel()[k]
	var = covars.ravel()[k]
	sigma = math.sqrt(var)
	ax.plot(x,max(smooth_h)*weights.ravel()[k]*stats.norm.pdf(x,mean,sigma) + min(smooth_h), c=color[k])

ax.plot(x, smooth_h, 'k')

plt.show()
