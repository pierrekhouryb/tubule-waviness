import sys
import lib.czifile as cz
import lib.detect_peaks
import numpy
import pylab as pl
import scipy as scipy
import scipy.signal as signal
from sklearn import mixture
import math

#General processing
def zProjectStackUsingMIP(stack):
	# From: http://imagejdocu.tudor.lu/doku.php?id=gui:image:stacks
	# Maximum Intensity projection (Max) creates an output image each of whose
	# pixels contains the maximum value over all images in the stack at the
	# particular pixel location.
	return numpy.amax(stack, 2)

def createMaskBasedOnEnergy(energy,threshold):
	return createMask(rescale(energy,0,1),threshold)

def createMask(image, t):
	mask = numpy.zeros(image.shape)
	mask[image > t] = 1.0
	return mask

def normalizehistogram(curve):

	return normcurve

def smoothdatacurve(curve, method = 'S-G', window_size = 11, order = 2):
	if(method == 'S-G'):
		smoothedcurve = signal.savgol_filter(curve, window_size, order)
	elif(method == 'MWA'):
		smoothedcurve = numpy.convolve(curve, numpy.ones((window_size,))/window_size, mode='valid')
	return smoothedcurve

def histogramtocurve(h, c):
	if(len(h) != len(c)):
		raise

	h_int = h.astype(int)
	hcurve = numpy.zeros((sum(h_int),1))

	kstart = 0
	idx = 0
	for k in h_int:
		hcurve[kstart:(kstart+k-1)] = c[idx]
		idx += 1
		kstart += (k - 1)

	return hcurve[:kstart]
#Tubule analysis
def computeStructureTensor1(Df):
	# Compute structure tensor of image f [x*y] and returns a 3D array
	# J: [4*x*y], with J[0,:,:] = fxx, J[1,:,:] = fxy, J[2,:,:] = fyx and
	# J[3,:,:] = fyy, where fij is th weighted inner product of the partial
	# spatial derivatives fi and fj along axes i and j resp. (here x and y).
	fx = Df[0,:,:]
	fy = Df[1,:,:]

	(nx, ny) = fx.shape

	# compute inner products
	fxx = numpy.zeros((nx,ny))
	fxy = numpy.zeros((nx,ny))
	fyy = numpy.zeros((nx,ny))
	for y in range(0,ny):
		for x in range(0, nx):
			fxx[x,y] = fx[x,y] * fx[x,y]
			fxy[x,y] = fx[x,y] * fy[x,y]
			fyy[x,y] = fy[x,y] * fy[x,y]

	gSig = 2.0
	wDim = 10.0

	fxx = gaussianFilter2D(fxx, wDim, gSig)
	fxy = gaussianFilter2D(fxy, wDim, gSig)
	fyy = gaussianFilter2D(fyy, wDim, gSig)
	#
	# pl.matshow(fxy)

	J = numpy.zeros((4,nx,ny))

	for y in range(0,ny):
		for x in range(0, nx):
			xx = fxx[x,y]
			xy = fxy[x,y]
			yy = fyy[x,y]
			J[0,x,y] = xx
			J[1,x,y] = xy
			J[2,x,y] = xy
			J[3,x,y] = yy

	return J;

def computeStructureTensor2(Df):
	# Compute structure tensor of image f [x*y] and returns a 3D array
	# J: [4*x*y], with J[0,:,:] = fxx, J[1,:,:] = fxy, J[2,:,:] = fyx and
	# J[3,:,:] = fyy, where fij is th weighted inner product of the partial
	# spatial derivatives fi and fj along axes i and j resp. (here x and y).
	fx = Df[0,:,:]
	fy = Df[1,:,:]

	(nx, ny) = fx.shape

	# compute inner products
	fxx = numpy.zeros((nx,ny))
	fxy = numpy.zeros((nx,ny))
	fyy = numpy.zeros((nx,ny))
	for y in range(0,ny):
		for x in range(0, nx):
			fxx[x,y] = fx[x,y] * fx[x,y]
			fxy[x,y] = fx[x,y] * fy[x,y]
			fyy[x,y] = fy[x,y] * fy[x,y]

	gSig = 1.0
	fxx = gaussianFilter2DCascade(fxx, gSig)
	fxy = gaussianFilter2DCascade(fxy, gSig)
	fyy = gaussianFilter2DCascade(fyy, gSig)

	J = numpy.zeros((4,nx,ny))

	for y in range(0,ny):
		for x in range(0, nx):
			xx = fxx[x,y]
			xy = fxy[x,y]
			yy = fyy[x,y]
			J[0,x,y] = xx
			J[1,x,y] = xy
			J[2,x,y] = xy
			J[3,x,y] = yy

	return J;

def analyzeStructureTensor(J):
	# Analyze structure tensor J [4*x*y] and returns a 3D array
	# M: [3*x*y], with M[0,:,:] = theta, M[1,:,:] = E, M[2,:,:] = C
	# where theta, E and C are the orientation, energy and coherency resp.
	(nd, nx, ny) = J.shape

	M = numpy.zeros((3,nx,ny))

	for y in range(0,ny):
		for x in range(0, nx):
			xx = J[0,x,y]
			xy = J[1,x,y]
			# yx = J[2,x,y]
			yy = J[3,x,y]
			M[0,x,y] = computeOrientationInternal(xx,xy,yy)
			M[1,x,y] = computeEnergyInternal(xx,yy)
			M[2,x,y] = computeCoherencyInternal(xx,xy,yy)

	return M;

def computePartialSpatialDerivatives(f, method, normalize):
	if(method == 'finiteDifferenceGradient'):
		# Compute partial spatial derivatives of f based on the Finite Difference
		# Gradient.
		(nx, ny) = f.shape
		sLoG = 0.0
		Df = numpy.zeros((2,nx,ny))

		log = calculateLaplacianOfGaussian(f,sLoG)

		for x in range(0,nx):
			colin = log[x,:]
			for y in range(1,ny-1):
				Df[0,x,y] = colin[y - 1] - colin[y + 1]

		for y in range(0,ny):
			rowin = log[:,y]
			for x in range(1,nx-1):
				Df[1,x,y] = rowin[x - 1] - rowin[x + 1]

		if(normalize):
			Df[0,:,:] = rescale(Df[0,:,:],0.0,1.0)
			Df[1,:,:] = rescale(Df[1,:,:],0.0,1.0)

		return Df;
	else:
		return 0

def processImage(img):
	# compute spatial derivatives
	Df = computePartialSpatialDerivatives(img,'finiteDifferenceGradient',False)
	# compute structure tensor
	J = computeStructureTensor1(Df)
	# analyze structure tensor
	M = analyzeStructureTensor(J)
	# output metrics
	return M

def computeMainOrientations(orientation, mask):
	h, binEdges = numpy.histogram(orientation.ravel(), bins=256, range=(numpy.min(orientation), numpy.max(orientation)), weights=mask.ravel())
	bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
	samples = numpy.transpose(numpy.vstack((bincenters, h)))
	print samples.shape
	gmix = mixture.GMM(n_components=1, covariance_type='full')
	gmix.fit(samples)
	print gmix.means_

def analyzeMetrics(metrics):
	orientation = metrics[0,:,:]
	energy = metrics[1,:,:]
	coherency = metrics[2,:,:]

	maskedEnergy = createMaskBasedOnEnergy(energy, 0.02)
	weightedMask = maskedEnergy * coherency

	raw_h, binEdges = numpy.histogram(orientation.ravel(), bins=256, \
	                                                   range=(numpy.min(orientation), numpy.max(orientation)), \
	                                                   weights=weightedMask.ravel())
	bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
	smooth_h = smoothdatacurve(raw_h, window_size = 21, order = 2)
	peaks = lib.detect_peaks.detect_peaks(smooth_h, mph=0.6*max(smooth_h), mpd=10, show=False)
	peaks = bincenters[peaks]
	raw_h_with_axes = numpy.zeros((2,len(raw_h)))
	raw_h_with_axes[0,:] = bincenters
	raw_h_with_axes[1,:] = raw_h

	return [peaks, raw_h_with_axes]

# Metrics
def computeOrientationInternal(xx,xy,yy):
	return 0.5 * math.atan2(2 * xy, (yy - xx))

def computeEnergyInternal(xx,yy):
	return xx + yy

def computeCoherencyInternal(xx,xy,yy):
	epsilon = 0.01
	return math.sqrt((yy - xx) * (yy - xx) + 4.0 * xy * xy) / (xx + yy + epsilon)

def calculateLaplacianOfGaussian(f,sLoG):
	if(sLoG > 0.0):
		return scipy.ndimage.filters.gaussian_laplace(f, sLoG).astype('float64')
	else:
		return f.astype('float64')

def computeError(exp, the, method):
	if(method=='absDifference'):
		return numpy.abs(exp-the)
	if(method=='percentError'):
		epsilon = 0.01
		return 100*numpy.abs(exp-the)/(numpy.abs(the)+epsilon)

# Operators
def gaussianFilter2D(fi,hsize,sig):
	h = matlab_style_gauss2D((hsize,hsize),sig)
	return scipy.ndimage.filters.convolve(fi, h)

def matlab_style_gauss2D(shape=(3,3),sigma=0.5):
	"""
	2D gaussian mask - should give the same result as MATLAB's
	fspecial('gaussian',[shape],[sigma])
	"""
	m,n = [(ss-1.)/2. for ss in shape]
	y,x = numpy.ogrid[-m:m+1,-n:n+1]
	h = numpy.exp( -(x*x + y*y) / (2.*sigma*sigma) )
	h[ h < numpy.finfo(h.dtype).eps*h.max() ] = 0
	sumh = h.sum()
	if sumh != 0:
		h /= sumh
	return h

def cast16to8(image, display_min, display_max): # copied from Bi Rico
	# Here I set copy=True in order to ensure the original image is not
	# modified. If you don't mind modifying the original image, you can
	# set copy=False or skip this step.
	image = numpy.array(image, copy=True)
	image.clip(display_min, display_max, out=image)
	image -= display_min
	image //= (display_max - display_min + 1) / 256.
	return image.astype(numpy.uint8)

def rescale(image, min_level, max_level):
	return (image - numpy.min(image))/(numpy.max(image) - numpy.min(image))

# Gaussian filtering
def gaussianFilter2DCascade(image, sigma):
	(nx, ny) = image.shape
	s2 = sigma * sigma
	pole = 1.0 + 3.0 / s2 - math.sqrt(9.0 + 6.0 * s2) / s2

	for x in xrange(0,nx):
		image[x, :] = convolveIIR_TriplePole(image[x], pole)

	row = numpy.zeros(nx)
	for y in xrange(0,ny):
		for x in xrange(0,nx):
			row[x] = image[x, y]
		row = convolveIIR_TriplePole(row, pole)
		for x in xrange(0,nx):
			image[x,y] = row[x]

	return image;

def convolveIIR_TriplePole(signal, pole):
	l = len(signal)
	N = 9
	Lambda = 1.0
	output = numpy.zeros(l)

	for k in xrange(0,N):
		Lambda = Lambda * (1.0 - pole) * (1.0 - 1.0 / pole);

	for n in xrange(0,l):
		output[n] = signal[n] * Lambda

	for k in xrange(0,N):
		output[0] = getInitialCausalCoefficientMirror(output, pole)
		for n in xrange(1,l):
			output[n] += pole * output[n-1]
		output[l-1] = getInitialAntiCausalCoefficientMirror(output, pole)
		for n in xrange(l - 2, -1, -1):
			output[n] = pole * (output[n+1] - output[n])

	return output;

def getInitialCausalCoefficientMirror(c, z):
	return (z * c[len(c) - 2] + c[len(c) - 1]) * z / (z*z - 1.0)

def getInitialAntiCausalCoefficientMirror(c, z):
	tolerance = 1.0E-5
	z1 = z
	zn = math.pow(z, len(c) - 1)
	Sum = c[0] + zn * c[len(c) - 1]
	horizon = len(c)

	if tolerance > 0.0:
		horizon = 2 + int(math.log(tolerance) / math.log(abs(z)))
		if horizon < len(c):
			horizon = horizon
		else:
			horizon = len(c)

	zn *= zn

	for n in xrange(1,horizon - 1):
		zn /= z
		Sum += (z1 + zn) * c[n]
		z1 *= z

	return Sum / (1.0 - math.pow(z, 2 * len(c) - 2))
