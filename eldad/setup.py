from distutils.core import setup
from Cython.Build import cythonize
import numpy
# import cv2

setup(
    ext_modules = cythonize("ridge_directed_ring_detector.pyx"),
    include_dirs=[numpy.get_include()]
)
