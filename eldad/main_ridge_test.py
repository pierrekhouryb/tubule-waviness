##
import os #,sys
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
except KeyError:
    user_paths = []
print(user_paths)
import numpy as np
from matplotlib import pyplot as plt
import platform
from datetime import datetime as dt
import pandas as pd
from glob import glob

hostname = platform.node()

print('This cell was run last at %s on \n\t%s' % ( dt.now(), hostname))


# import javabridge as jv
# import bioformats as bf
# jv.start_vm(class_path=bf.JARS, max_heap_size='8G')

##  load (and compile if necessary) the ridge_directed_ring_detector
import pyximport; pyximport.install()
import ridge_directed_ring_detector as ring_detector


# from xml.etree import ElementTree as ETree
#
# DEFAULT_DIM_ORDER = 'tzyxc'
# from xml import etree as et
#
# def parse_xml_metadata(xml_string, array_order=DEFAULT_DIM_ORDER):
#     """Get interesting metadata from the LIF file XML string.
#     Parameters
#     ----------
#     xml_string : string
#         The string containing the XML data.
#     array_order : string
#         The order of the dimensions in the multidimensional array.
#         Valid orders are a permutation of "tzyxc" for time, the three
#         spatial dimensions, and channels.
#     Returns
#     -------
#     names : list of string
#         The name of each image series.
#     sizes : list of tuple of int
#         The pixel size in the specified order of each series.
#     resolutions : list of tuple of float
#         The resolution of each series in the order given by
#         `array_order`. Time and channel dimensions are ignored.
#     """
#     array_order = array_order.upper()
#     names, sizes, resolutions = [], [], []
#     spatial_array_order = [c for c in array_order if c in 'XYZ']
#     size_tags = ['Size' + c for c in array_order]
#     res_tags = ['PhysicalSize' + c for c in spatial_array_order]
#     metadata_root = et.ElementTree.fromstring(xml_string)
#     for child in metadata_root:
#         if child.tag.endswith('Image'):
#             names.append(child.attrib['Name'])
#             for grandchild in child:
#                 if grandchild.tag.endswith('Pixels'):
#                     att = grandchild.attrib
#                     sizes.append(tuple([int(att[t]) for t in size_tags]))
#                     resolutions.append(tuple([float(att[t])
#                                               for t in res_tags]))
#     return names, sizes, resolutions
#
#
# def metadata_summary(names, sizes, resolutions):
#     header = '{} -- metadata summary'.format(names[0])
#     print (header)
#     print ('='*len(header))
#
#     print ('\nsizes:')
#     for d,s in zip(DEFAULT_DIM_ORDER, sizes[0]):
#         print ('\t{} :\t{:3}'.format(d,s))
#
#     print ('\nmicrons to pixels ratio:')
#     for d,s in zip('zyx', resolutions[0]):
#         print ('\t{} :\t{:.3f} um/px'.format(d,s))
