requires("1.47j");


sigma =newArray(1,2,3);     // largeur de la gaussienne (en pixels)
largeur = 1.5;   // largeur de la zone de calcul (en sigma)
longueur = 4.0;  // longueur de la zone de calcul (en sigma)
nbAngles = 7.0; // Nombre d'angles considérés

// initialisation du plugin Masque
run("Barreau ");
  startImageID = getImageID();

  // calcul des correlations
  Ext.initBarreau();
  Ext.setBarreauParams(sigma,largeur,longueur,nbAngles);
  Ext.doBarreau(0); // le paramètre permet d'indiquer le nombre de largeurs à calculer 
  // enregistrement des images d'angle et de corrélation
  selectWindow("angle");
  angleID = getImageID();
  selectWindow("correlation");
  correlationID = getImageID();
  selectWindow("largeur");
  largeurID = getImageID();
  selectWindow("stackCorrelation");
  stackCorrelationID = getImageID();

  // segmentation des filaments
  selectImage(correlationID);
  w = getWidth();
  h = getHeight();
  makeRectangle(w/3, h/3, w/3, h/3); // modifier pour prendre vraiment la partie centrale de l'image !
  selectImage(stackCorrelationID);
//  run("Duplicate...", "qsjdkfl");
  setThreshold(0.25, 1.0);
  run("Convert to Mask", "background=Dark black");
  setAutoThreshold("Otsu dark stack");
  run("Analyze Particles...", "size="+20+"-Infinity circularity=0.00-0.50 show=Masks exclude clear in_situ stack");
  run("Invert LUT");
  run("Z Project...", "projection=[Max Intensity]");

