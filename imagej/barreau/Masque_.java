import java.util.Arrays;
import ij.*;
import ij.macro.*;
import ij.process.*;
import ij.gui.*;

import java.awt.*;
import ij.plugin.*;
import ij.plugin.filter.*;
import ij.plugin.frame.*;

public class Masque_ implements PlugIn, MacroExtension {
	static ImagePlus imp;
	static ImageProcessor ip;

	static boolean verbose = true;

	/** paramètres du calcul ****************************/
	// on pondère par l'inverse du rayon  
	static double largeur = 2.0;         // lageur de la gaussienne
	static double factLargeur = 1.5;     // largeur de la zone d'exploration (en unité de largeur)
	static double factLongueur = 4.0;    // longueur de la zone de calcul (en unité de largeur)
	static int    nbAngle  = 5;          // nombre d'angles à considérer (les angles sont pris entre 0 et 180°)
                                       // en fait le calcul donne le résultat pour 2*nbAngle+1
	/****************************************************/
    

	private ExtensionDescriptor[] extensions = {
			ExtensionDescriptor.newDescriptor("initMasque", this),
			ExtensionDescriptor.newDescriptor("setMasqueParams", this, ARG_NUMBER, ARG_NUMBER, ARG_NUMBER, ARG_NUMBER),
			ExtensionDescriptor.newDescriptor("doMasque", this, ARG_NUMBER),
			ExtensionDescriptor.newDescriptor("testMasque", this )
	};

	public ExtensionDescriptor[] getExtensionFunctions() {
		return extensions;
	}

	public String handleExtension(String name, Object[] args) {
		if (name.equals("initMasque")) {								// initMasque ()
			initMasque();
		} else if  (name.equals("setMasqueParams")){				// setMasqueParams(double largeur, double factLargeur, double factLongueur, double nbAngle)
			largeur = ((Double)args[0]);
			factLargeur = ((Double)args[1]);
			factLongueur = ((Double)args[2]);
			nbAngle = ((Double)args[3]).intValue();
			setMasqueParams(largeur, factLargeur, factLongueur, nbAngle);
		} else if  (name.equals("doMasque")){				// doMasque(slice)
			int slice = ((Double)args[0]).intValue();
			doMasque(slice);
		} else if  (name.equals("testMasque")){
			testMasque();
		}
		return null;
	}
	

	private static void initMasque(){
		imp = IJ.getImage();
		if (imp == null ){
			IJ.error("Il doit y avoir une image ouverte");
			return;
		}
/*
		if (imp.getStackSize()<=1){
			IJ.error("L'image active doit-être une pile d'image (stack)");
			return;
		}
*/		
		ip = imp.getProcessor();
	}

	
	@Override
	public void run(String arg) {
		
		
		if (IJ.macroRunning()){
			Functions.registerExtensions(this);
		} else {
			// début -- petit bout de code pour fermer l'éventuelle fenêtre 
			// d'erreurs ou de warning à la compilation
			Frame frame = WindowManager.getFrame("Errors");
			if (frame!=null && (frame instanceof Editor)) {
				((Editor)frame).close();
			}
			// fin -- petit bout de code pour fermer l'éventuelle fenêtre 
			// d'erreurs ou de warning à la compilation
			
			GenericDialog gd = new GenericDialog("Masque Options");
			gd.addNumericField("Largeur : ", largeur, 3);
			gd.addNumericField("factLargeur : ", factLargeur, 3);
			gd.addNumericField("factLongueur : ", factLongueur, 3);
			gd.addNumericField("nbAngle : ", nbAngle, 0);
			gd.showDialog();
			if (gd.wasCanceled())
				return;
			largeur = gd.getNextNumber();
			factLargeur = gd.getNextNumber();
			factLongueur = gd.getNextNumber();
			nbAngle = (int)gd.getNextNumber();

			initMasque();
			doMasque(-1);
		}
	} // public voir run(String arg)

	private static void setMasqueParams(double largeur_, double factLargeur_, double factLongueur_, int nbAngle_){
		largeur = largeur_;
		factLargeur = factLargeur_;
		factLongueur = factLongueur_;
		nbAngle = nbAngle_;
	}
	
/*
 *  Attention, cette fonction calcule le point d'intersection entre un bipoint et
 *  une droite d'abcisse x 
 *  mais elle n'est pas générale dans la mesure où elle est faite pour ne pas
 *  renvoyer de solution lorsque la droite est orientée suivant l'axe des x
 *  Cela est utile pour cette routine mais n'est pas complètement général.	
 */
	private static Point intersect(Point p1, Point p2, double x){
		if (p2.x == p1.x){
//			if (x == p1.x) return p1;
			return null;
		}
		double lambda = (x-p1.x)/(p2.x-p1.x);
//IJ.log("lambda "+lambda);		
		if ( (lambda >=0) && (lambda <=1) ){ 
			double y = p1.y+ lambda*(p2.y-p1.y);
			return new Point(x, y, p1.theta, false);
		}
		return null;
	}

	private static void doMasque(int slice){
// la version vraiment moche qui ne fait rien comme il faut		
		int w = imp.getWidth();
		int h = imp.getHeight();


// création des images pour rendre les résultats...
		ImagePlus impC = IJ.createImage("correlation", "32-bit", w, h, 1);
		ImageProcessor ipC = impC.getProcessor();
		float [] arrC = (float [])ipC.getPixels();

		ImagePlus impA = IJ.createImage("angle", "32-bit", w, h, 1);
		ImageProcessor ipA = impA.getProcessor();
		float [] arrA = (float [])ipA.getPixels();

    ImagePlus impR = IJ.createImage("res", "32-bit", w, h, 1);
    ImageProcessor ipR = impR.getProcessor();
    float [] arrR = (float [])ipR.getPixels();

    ImagePlus impS = IJ.createImage("stackCorrelation", "32-bit", w, h, 2*nbAngle+1);
    ImageStack iS = impS.getImageStack();
    
		double W = largeur * factLargeur;
		double H = largeur * factLongueur;
		double sigma2 = largeur*largeur;
		int lE = (int)Math.ceil(Math.sqrt(H*H+W*W));
		
		Correl cor[][] = new Correl[w][h];
		for(int i=lE; i<w-lE; i++){
			for (int j=lE; j<h-lE; j++){
				cor[i][j]=new Correl();
			}
		}
		
		// on se limite à des rotation en pi/2 ce qui permet de contrôler le point le plus à gauche
		int compteur =0;
    		int thetaIndex=0;
		for(double theta = 0 ; theta <= Math.PI/2.0; theta += Math.PI/(2*nbAngle)){
			
			Point.cos = Math.cos(theta);
			Point.sin = Math.sin(theta);
			//IJ.log("theta = "+theta);
			compteur++;
			
			Point hg = new Point(-W, +H, theta, true); // haut gauche
			Point hd = new Point(+W, +H, theta, true); // haut droit
			Point bg = new Point(-W, -H, theta, true); // bas gauche
			Point bd = new Point(+W, -H, theta, true); // bas droit

			
			for(int i=lE; i<w-lE; i++){
				for (int j=lE; j<h-lE; j++){
					cor[i][j].s=0;
					cor[i][j].s2=0;
					cor[i][j].sg=0;
					cor[i][j].sp=0;
					cor[i][j].sp2=0;
					cor[i][j].spg=0;
					cor[i][j].g=0;
					cor[i][j].count=0;
				}
			}
			
			// le point le plus à gauche est forcément le point bg
			// le point le plus à droite est forcément le point hd
			// on parcourt horizontalement entre les points extrêmes
			for (int di = (int)(Math.ceil(bg.x)); di <= (int)(Math.floor(hd.x)); di++){
				Point p1 = intersect(bg,hg,di);
				Point p2 = intersect(hg,hd,di);
				Point p3 = intersect(hd,bd,di);
				Point p4 = intersect(bd,bg,di);
				
				
				// calcul des points extrêmes verticalement pour le di en cours...
				double miny = +10000;
				double maxy = -10000;
				if (p1 != null) {
					if (p1.y < miny) miny=p1.y;
					if (p1.y > maxy) maxy=p1.y;
				}
				if (p2 != null)  {
					if (p2.y < miny) miny=p2.y;
					if (p2.y > maxy) maxy=p2.y;
				}
				if (p3 != null)  {
					if (p3.y < miny) miny=p3.y;
					if (p3.y > maxy) maxy=p3.y;
				}
				if (p4 != null)  {
					if (p4.y < miny) miny=p4.y;
					if (p4.y > maxy) maxy=p4.y;
				}
			

				// on fait le parcours vertical
				for (int dj = (int)Math.ceil(miny); dj<=(int)Math.floor(maxy); dj++){
					
					Point p = new Point(di, dj, theta, false);
					
/* pour vérifier que c'est bien avec les bonnes zones que l'on travaille !
					if (compteur == slice){
						ipC.putPixelValue(w/2+di, h/2+dj, 128);
						ipA.putPixelValue(w/2+(int)p.i, h/2+(int)p.j, 128);
						IJ.log(""+p);
					}
*/
					for(int i=lE; i<w-lE; i++){
						for (int j=lE; j<h-lE; j++){
							double v = ip.getPixelValue(i+di, j+dj);
							double vp = ip.getPixelValue(i-di, j+dj);
							double g = Math.exp(-(p.i*p.i)/sigma2);
							cor[i][j].s   += v;
							cor[i][j].s2  += v*v;
							cor[i][j].sg  += v * g;
							cor[i][j].sp   += vp;
							cor[i][j].sp2  += vp*vp;
							cor[i][j].spg  += vp * g;							
							cor[i][j].g   += g;
							cor[i][j].count++;
						}
					}
				} // for (dj)
			}// for (di)
			
			for(int i=lE; i<w-lE; i++){
				for (int j=lE; j<h-lE; j++){
					double sgm = cor[i][j].sg/cor[i][j].count;
					double sm  = cor[i][j].s/cor[i][j].count;
					double s2m = cor[i][j].s2/cor[i][j].count;
					double gm  = cor[i][j].g/cor[i][j].count;
					double spgm = cor[i][j].spg/cor[i][j].count;
					double spm  = cor[i][j].sp/cor[i][j].count;
					double sp2m = cor[i][j].sp2/cor[i][j].count;
					
					double corr = (sgm - sm*gm); 
					double corrp = (spgm - spm*gm); 
					if (corr >cor[i][j].maxCor) {
						cor[i][j].maxCor=corr;
						cor[i][j].theta=theta;
            cor[i][j].thetaIndex = thetaIndex; 
					}
					if (corrp >cor[i][j].maxCor) {
						cor[i][j].maxCor=corrp;
						cor[i][j].theta=-theta;
            cor[i][j].thetaIndex = thetaIndex + nbAngle;
					}
				}
			}
      thetaIndex++;
 		} // for (theta)

		for(int i=lE; i<w-lE; i++){
			for (int j=lE; j<h-lE; j++){
				ipC.putPixelValue(i, j, cor[i][j].maxCor);
				ipA.putPixelValue(i, j, cor[i][j].theta);
        iS.setVoxel(i, j, cor[i][j].thetaIndex, cor[i][j].maxCor);
			}
		}
   
		impC.show();
		impC.updateAndDraw();
		impC.resetDisplayRange();
		
		impA.show();
		impA.updateAndDraw();
		impA.resetDisplayRange();

    impS.show();
    impS.updateAndDraw();
    impS.resetDisplayRange();


	} // doMasque

	private static void testMasque(){
		IJ.log("test 2 du module membranes");
		IJ.log("largeur = "+largeur);
		IJ.log("factLargeur = "+factLargeur);
		IJ.log("factLongueur = "+factLongueur);
		IJ.log("nbAngle = "+nbAngle);

		
		Point p1 = new Point(1.0,1.0,Math.PI/2.0, true);
		Point p2 = new Point(-1.0,1.0,Math.PI/2.0, true);
		Point p3 = new Point(1.0, 1.0, Math.PI/2.0, false);
		
		IJ.log(p1.toString());
		IJ.log(p2.toString());
		IJ.log(p3.toString());
		
		
//voici ce qu'il faut faire pour récupérer des valeurs passées dans un tableau !!!      
//  Object [] obj = (Object [])args[0];
//  IJ.log("premiere "+((Double)obj[0]).intValue());
//  IJ.log("deuxième "+((Double)obj[1]).intValue());
//  					
	}

	
	
} // class Masque_


class Point{
	
	static double cos, sin;
	
	public double x;  // les coordonnées du point une fois tourné
	public double y; 
	
	public double i;  // les coordonnées avant rotation
	public double j;
	
	public double theta; // l'angle de rotation
	
	private void PointDirect(double i, double j, double theta){
		this.i =i;
		this.j =j;
		this.theta =theta;
		
		
		this.x =  cos*i + sin*j;
		this.y = -sin*i + cos*j;
	}
	
	public void PointReverse(double x, double y, double theta){
		this.x =x;
		this.y =y;
		this.theta =theta;
				
		this.i =  cos*x - sin*y;
		this.j =  sin*x + cos*y;
	}

	public Point(double v1, double v2, double theta, boolean direct){
		if (direct){
			PointDirect(v1,v2, theta);
		} else {
			PointReverse(v1,v2, theta);
		}
	}
	
	public String toString(){
		return ("("+i+" , "+j+") <- "+theta+" -> ("+x+" , "+y +")");
	}
} // class Point


class Correl {
	public double s=0;
	public double s2=0;
	public double sg=0;
	public double sp=0;
	public double sp2=0;
	public double spg=0;
	public double g=0;
	public int count=0;
	
	public double maxCor =-1000;
	public double theta =0;
  public int thetaIndex = 0;
  
}
