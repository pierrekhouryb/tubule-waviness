sigma = 1;     // largeur de la gaussienne (en pixels)
largeur = 1.5;   // largeur de la zone de calcul (en sigma)
longueur = 4.0;  // longueur de la zone de calcul (en sigma)
nbAngles = 5.0; // Nombre d'angles considérés

input = "/home/olivier/tmp/Sylvie/img/";
output = "/home/olivier/tmp/Sylvie/";

// initialisation du plugin Masque
run("Masque ");


requires("1.42j");

setBatchMode(true);
list = getFileList(input);
print("============== début ==========");
for (i = 0; i < list.length; i++) {
  print ("->  "+(i+1)+"/"+list.length+" : "+list[i]);
  action(input, output, list[i]);
}
setBatchMode(false);
print("============== fin ==========");

function action(input, output, filename){
  // ouverture de l'image
  open(input+filename);

  // calcul des correlations
  Ext.initMasque();
  Ext.setMasqueParams(sigma,largeur,longueur,nbAngles);
  startImageID = getImageID();
  Ext.doMasque(3);  

  // segmentation des filaments
  selectWindow("correlation");
  w = getWidth();
  h = getHeight();
  makeRectangle(w/4, h/4, w/2, h/2); // modifier pour prendre vraiment la partie centrale de l'image !
  setAutoThreshold("Otsu dark stack");
  getThreshold(lower, upper);
  close(); // on ferme l'image de correlation
  selectWindow("stackCorrelation");
  print(lower, upper);
  setThreshold(lower, upper);
  run("Convert to Mask", "background=Dark black");
  setAutoThreshold("Otsu dark stack");
  run("Analyze Particles...", "size=30-Infinity circularity=0.00-0.50 show=Masks exclude clear in_situ stack");
  run("Invert LUT");
  run("Z Project...", "projection=[Max Intensity]");

  // enregistrement de l'image segmentée
  saveAs("Tiff", output+'/seg/'+filename);

  // on remet le tout dans la stack de départ
  run("Select All");
  run("Copy");
  selectImage(startImageID);
  run("8-bit");
  run("Add Slice"); // on fabrique la stack qui va bien
  setSlice(2);
  run("Paste");
  run("Make Montage...", "columns=2 rows=1 scale=1 first=1 last=2 increment=1 border=0 font=12");
  // enregistrement de la pile d'image résultat !
  saveAs("Tiff", output+'/res/'+filename);
  
  // on ferme tout ce qui a besoin de l'être !
  close();
  close();
  close();
  close();
  close();
}