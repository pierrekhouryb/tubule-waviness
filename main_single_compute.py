import os
import sys
import numpy
import lib.ioutils as ioutils
import imageutils
import plotutils
import imageutils_tests
import scipy
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import math

#Make sure we have the right inputs
if len(sys.argv) != 2:
	print('Usages are: python main.py arg1 arg2')
	sys.exit()

#Start
print('Running %s' % sys.argv[0])
fin = sys.argv[1]

print('input: %s' % fin)

datadir = 'images/'

# with cz.CziFile('images/mchtua5c1rectangle.czi') as czi:
filename = datadir + fin
image = ioutils.loadandpreprocessfromfile(filename)

M = imageutils.processImage(image)
orientation = M[0,:,:]
energy = M[1,:,:]
coherency = M[2,:,:]

[p, d] = imageutils.analyzeMetrics(M)

plotutils.plotImage(image, 'image')
plotutils.plotImage(orientation, 'orientation')
plotutils.plotImage(energy, 'energy')
plotutils.plotImage(coherency, 'orientation')

energyMask = imageutils.createMaskBasedOnEnergy(energy, 0.02)
plotutils.plotHisto(orientation * 180/math.pi, energyMask * coherency)


# ioutils.writef8(orientation, datadir + 'processed/' + ioutils.getfilenamewithoutextension(fin) + '_orient')
# ioutils.writef8(energy, datadir + 'processed/' + ioutils.getfilenamewithoutextension(fin) + '_energy')
# ioutils.writef8(coherency, datadir + 'processed/' + ioutils.getfilenamewithoutextension(fin) + '_cohere')

plt.show()
