import os
import sys
import numpy
import lib.ioutils as ioutils
import imageutils
import plotutils
import imageutils_tests
import scipy
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import math

#Make sure we have the right inputs
if len(sys.argv) != 2:
	print('Usages are: python main.py arg1 arg2')
	sys.exit()

#Start
print('Running %s' % sys.argv[0])
fin = sys.argv[1]

print('input: %s' % fin)

datadir = 'images/'

# with cz.CziFile('images/mchtua5c1rectangle.czi') as czi:
filename = datadir + fin
image = ioutils.loadandpreprocessfromfile(filename)

# compute spatial derivatives
Df = imageutils.computePartialSpatialDerivatives(image,'finiteDifferenceGradient',False)

# compute structure tensor
J = imageutils.computeStructureTensor1(Df)

# analyze structure tensor
M = imageutils.analyzeStructureTensor(J)
orientation = M[0,:,:]
energy = M[1,:,:]
coherency = M[2,:,:]

tests = imageutils_tests.Imageutils_tests(os.path.splitext(fin)[0])
print(tests.validateZProjectionUsingMIP(image))
print(tests.validatePartialSpatialDerivativeX(Df[0,:,:]))
print(tests.validatePartialSpatialDerivativeY(Df[1,:,:]))
print(tests.validateOrientation(orientation,False))
print(tests.validateEnergy(energy,False))
print(tests.validateCoherency(coherency,False))

energyMask = imageutils.createMaskBasedOnEnergy(energy, 0.02)
plotutils.plotHisto(orientation * 180/math.pi, energyMask * coherency)

pylab.ioff()
plt.show()
