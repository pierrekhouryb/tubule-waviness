import os
import sys
import json
import numpy
import lib.ioutils as ioutils
import imageutils
import plotutils
import imageutils_tests
import scipy
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import math

debug = 1
fin = fout = ""
#Make sure we have the right inputs
if debug == 1:
	fin = "./data/raw/FilesWithPaths.txt"
	fout = "proctest-0"
elif len(sys.argv) != 2:
	print('Usages are: python main_batch_process.py file_list_in.txt procdir')
	sys.exit()
else:
	#Start
	print('Running %s' % sys.argv[0])
	fin = sys.argv[1]
	fout = sys.argv[2]

print('input list: %s' % fin)
print('ouput dir: %s' % fout)

datadir = os.path.dirname(fin)

with open(fin) as f:
	filelist = [datadir + line.rstrip('\n')[1:] for line in f]

print(len(filelist))

filelist = filelist[0:20]

print os.path.isfile("./data/raw/.DS_Store")

global_index = 0
errorlist = []
for fn in filelist:
	# initialize error to 0.
	# (0:success; 1:bad filename, 2:bad image, 3:bad computation, 4:bad analysis)
	error = 0
	filename = fn
	if(not os.path.isfile(filename) or ioutils.getfilenameextension(filename) != ".czi"):
		error = 1

	# read image
	if(error == 0):
		try:
			rawimage = ioutils.loadandpreprocessfromfile(filename)
		except:
			error = 2

	# compute: raw => [orientation, coherency, energy]
	if(error == 0):
		try:
			metrics = imageutils.processImage(rawimage)
		except:
			error = 3

	# analyze: [orientation, coherency, energy] => [peaks, distribution]
	peaks = []
	if(error == 0):
		try:
			[peaks, distribution] = imageutils.analyzeMetrics(metrics)
		except:
			error = 4
	# update output
	errorlist.append((global_index,error,fn))
	global_index += 1
	print(peaks)

print(len(errorlist))

with open("data/test/test.json",'w+') as output_file:
	for line in errorlist:
		s = r'{{"index":{index},"status":{status},"url":"{url}"}}'\
				.format(index=str(line[0]),status=str(line[1]),url=line[2])
		output_file.write("%s\n" % s)

# with open("data/test/test.json") as test_file:
#     for line in test_file:
#         test_data = json.loads(line)
#         print test_data["url"]
