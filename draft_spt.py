import os
import sys
import numpy as np
import lib.ioutils as ioutils
import imageutils
import plotutils
import imageutils_tests
import scipy
import matplotlib.pyplot as plt
import matplotlib.pylab as pylab
import math
import sklearn.metrics
from skimage.restoration import unwrap_phase
import scipy.ndimage.filters as filters

def calculateEnergy(a, modified):
    ww = a.shape[0]
    hh = a.shape[1]
    ft = False
    centreX = ww / 2
    centreY = hh / 2
    result = np.zeros((2, ww, hh), dtype=complex)
    for cmp in range(0,2):
        component = np.real(a) if cmp == 0 else np.imag(a)
        noDC = component # assuming dc has already been subtracted
        h = np.zeros((ww, hh), dtype=complex)
        # change below operations on a grid
        for x in range(0,ww):
            iFactor = x - centreX
            for y in range(0,hh):
                rFactor = y - centreY
                pha = np.arctan2(iFactor, rFactor)
                c = np.cos(pha)
                s = np.sin(pha)
                h[x, y] = (1.0 if modified == True else np.sqrt(iFactor**2 + rFactor**2)) * np.complex(real=c, imag=s)
                pass
            pass
        pass

        fs = np.fft.fftshift(np.fft.fft2(noDC, s=None, axes=(0,1), norm=None))
        hfs = h * fs
        fhfs = np.fft.ifft2(np.fft.fftshift(hfs))
        hhfs = h * hfs
        fhhfs = np.fft.ifft2(np.fft.fftshift(hhfs))

        fs[centreX, centreY] = 0
        ss = np.fft.ifft2(np.fft.fftshift(fs))
        hfs = fhfs * fhfs

        fs = ss * fhhfs
        result[cmp] = hfs - fs

    return result[0] + result[1]

def addDislocation(a, order, xOffset, yOffset, use_abs):
    ww = a.shape[0]
    hh = a.shape[1]
    centreX = ww / 2 if ww % 2 == 0 else (ww - 1) / 2
    centreY = hh / 2 if hh % 2 == 0 else (hh - 1) / 2

    da = np.zeros_like(a)
    for x in range(0,ww):
        xx = x - centreX - xOffset
        for y in range(0,hh):
            yy = y - centreY - yOffset
            phase = order * np.arctan2(yy, xx)
            if(use_abs == True):
                phase = np.abs(phase)
            da[x,y] = a[x,y] * np.complex(np.cos(phase), np.sin(phase))
            pass
        pass
    return da

def demod_vortex(data, use_sel = True, unwrap = True):
    # Based on:
    # https://github.com/magister-ludi/eigenbroetler/blob/develop/cpp/source/complex_operations.cpp#L300
    # assume data has no DC - to do: add case ...
    ww = data.shape[0]
    hh = data.shape[1]
    noDC = data
    energy = calculateEnergy(data, True)

    # Uncomment below to plot real and imaginary part of the energy
    # import matplotlib.pyplot as plt
    # import miplotlib as mipl
    # fig, ax = plt.subplots(1,2)
    # mipl.miImshow(ax.ravel()[0], np.real(energy))
    # mipl.miImshow(ax.ravel()[1], np.imag(energy))
    # fig.suptitle('energy')
    # plt.draw()

    # The math below yield to polar uncertainties due to nature of orientation
    # pha = 0.5 * np.angle(energy)
    # Use below instead, that yields to wrapped phase
    pha = 0.5 * np.sqrt(np.power(np.angle(energy),2))
    nd = np.cos(pha) + 1j * np.sin(pha) # == exp[-iBETA]
    ff = np.fft.fftshift(np.fft.fft2(noDC, s=None, axes=(0,1), norm=None)) # FFT
    h2 = addDislocation(ff, 1, 0, 0, False) # X exp[iPHI]
    ff = np.fft.ifft2(np.fft.fftshift(h2)) # IFFT
    h2 = ff * nd # Vortex

    if(use_sel):
        for x in range(0, ww):
            for y in range(0,hh):
                nd[x,y] = np.complex(noDC[x,y], np.imag(h2[x,y]))
                pass
            pass
    else:
        nd = noDC + h2

    data_demod = np.abs(nd)
    if unwrap:
        phase_demod = unwrap_phase(np.angle(nd))
    else:
        phase_demod = np.angle(nd)
    # return (np.ndarray.astype(np.abs(nd), dtype='float32'), np.ndarray.astype(pha, dtype='float32'))
    return (np.ndarray.astype(data_demod, dtype='float32'), np.ndarray.astype(phase_demod, dtype='float32'))

#Make sure we have the right inputs
if len(sys.argv) != 2:
	print('Usages are: python main.py arg1 arg2')
	sys.exit()
else:
    fin = '/Users/pierrekhouryb/tubule-waviness/images/mchtua5c1rectangle.czi'

#Start
print('Running %s' % sys.argv[0])
fin = sys.argv[1]

print('input: %s' % fin)

datadir = 'images/'

# with cz.CziFile('images/mchtua5c1rectangle.czi') as czi:
filename = datadir + fin
image = ioutils.loadandpreprocessfromfile(filename)

energy = calculateEnergy(image, True)

plotutils.plotImage(image, 'image')
plotutils.plotImage(np.abs(energy),'energy magnitude')
plotutils.plotImage(np.angle(energy),'energy angle')

plt.show()
