import os
import os.path
import sys
import logging
import argparse
import ast
import warnings
import time
import datetime
import json
import math
import numpy as np
import pandas as pd
import glob2

logger = logging.getLogger(__name__)

def getTimeStamp():
    return datetime.datetime.now().strftime("%Y%m%d%H%M%S")

def parse_arguments():
    p = argparse.ArgumentParser()
    p.add_argument('-rootdir', help="Root Directory: for example ""C:\\Data\\Test\\A"".")
    p.add_argument('-outname', help="Output Filename (optional): for example ""result.txt"" (default).")
    p.add_argument('-datarow', help="Row of the data to extract: if -1, the first digit value in the col is returned.")
    p.add_argument('-datacol', help="Col of the data to extract: if -1, the first digit value in the row is returned.")

    args = p.parse_args()

    default_rootdir = "C:\\Data\\Test\\A"
    default_outname = getTimeStamp()+'_result.txt'
    default_datarow = -1
    default_datacol = -1

    if (args.rootdir is None):
        logger.info('* No arguments, using default hardcoded values')
        # Locate data
        rootdir = default_rootdir
        outname = default_outname
        datarow = default_datarow
        datacol = default_datacol
    else:
        rootdir = os.path.abspath(args.rootdir)
        if not os.path.isdir(rootdir):
            raise ValueError('{} is not a valid input path.'.format(rootdir))

        if not args.outname:
            outname = default_outname
        else:
            outname = getTimeStamp() + '_'+ args.outname

        if args.datarow is None:
            datarow = -1
        else:
            datarow = int(args.datarow)

        if args.datacol is None:
            datacol = -1
        else:
            datacol = int(args.datacol)


    logger.info('Input parameters:')
    logger.info(' - Root Directory: {}'.format(rootdir))
    logger.info(' - Output Filename: {}'.format(outname))
    logger.info(' - Data Row: {}'.format(datarow))
    logger.info(' - Data Col: {}'.format(datacol))

    return rootdir, outname, datarow, datacol

def getFirstSubdirAfterRoot(root, path):
    if root not in path:
        return None

    def splitPath(path):
        folders = []
        while 1:
            path, folder = os.path.split(path)
            if folder != "":
                folders.append(folder)
            else:
                if path != "":
                    folders.append(path)
                break
        folders.reverse()
        return folders

    sroot = splitPath(root)
    spath = splitPath(path)
    print(spath[0:len(sroot)+1])
    return os.path.join(*spath[0:len(sroot)+1])

def writeOutlist(outlist, resultfilename):
    logger.info('Writing ' + resultfilename)
    pd.DataFrame(outlist, columns=["values"]).to_csv(resultfilename, mode='a', index=False, header=False)

def main():
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(levelname)-5s] %(lineno)s | %(message)s',
                        datefmt='%H:%M:%S')

    rootdir, outname, datarow, datacol = parse_arguments()

    outlist = []
    curr_subdir = ''
    for filename in glob2.iglob(rootdir+'\\**\\*.txt'):
        # only treat files in subfolders, and ignore files in root
        # to avoid re-processing result files.
        if not os.path.dirname(filename) == rootdir and "extrema" in os.path.basename(filename):
            new_subdir = getFirstSubdirAfterRoot(rootdir, os.path.dirname(filename))# new file subdir
            if not curr_subdir:
                curr_subdir = new_subdir
            # if just entered a new subfolder, write results to the previous one first
            if not new_subdir == curr_subdir and len(outlist)>0:
                writeOutlist(outlist, os.path.join(curr_subdir, outname))
                curr_subdir = new_subdir
                outlist = []

            logger.info('Reading ' + filename)
            data = pd.read_csv(filename)
            try:
                if datarow>=0 and datacol>=0:
                    outlist.append(data.iat[datarow,datacol])
                elif datarow>=0 and datacol<0:
                    for i in range(data.shape[1]):
                        tmp = data.iat[datarow, i]
                        if not np.isnan(tmp):
                            outlist.append(tmp)
            except:
                logger.info('Could not find a value here....')

    # Write last subfolders
    if curr_subdir and len(outlist)>0:
        writeOutlist(outlist, os.path.join(curr_subdir, outname))


if __name__ == "__main__":
    main()
