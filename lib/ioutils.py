import os
import czifile as cz
import imageutils
from PIL import Image
import numpy

def getfilenameextension(filename):
	return os.path.splitext(filename)[1]

def getfilenamewithoutextension(filename):
	return os.path.splitext(filename)[0]

def loadandpreprocessfromfile(filename):
	try:
		ext = getfilenameextension(filename)
		if(ext == '.czi'):
			image = loadandpreprocessfromczi(filename)
		elif(ext == '.png'):
			image = loadandpreprocessfrompng(filename)
		else:
			raise Exception('Extension not recognized:' + ext)
	except Exception, e:
		print('Error trying to load image' + str(e))

	return image

def loadandpreprocessfrompng(filename):
	im = Image.open(filename)
	pix = im.load()
	image = numpy.zeros(im.size)
	for x in xrange(0, im.size[0]):
		for y in xrange(0,im.size[1]):
			image[x, y] = pix[x,y]
	return image

def loadandpreprocessfromczi(filename):
	with cz.CziFile(filename) as imagefile:
		# load fluo stack from czi file
		fluostack = loadfluostackfromcziobject(imagefile)
		# subtract background
		# http://imagejdocu.tudor.lu/doku.php?id=gui:process:subtract_background
		# z-project
		image = imageutils.zProjectStackUsingMIP(fluostack)
	return image

def loadfluostackfromcziobject(czi_object):
	# remove extra dimensions
	fluostack = numpy.squeeze(czi_object.asarray())
	# select the fluo channel (0: fluo, 1: brightfield)
	fluostack = fluostack[0,:,:,:]
	# permute so that the stack is [x,y,z] shaped
	fluostack = numpy.transpose(fluostack, (1,2,0))
	return fluostack;

def loadu2(filename):
	return numpy.fromfile(filename, dtype='>u2', sep="")

def loadf4(filename):
	return numpy.fromfile(filename, dtype='>f4', sep="")

def loadf8le(filename):
	return numpy.fromfile(filename, dtype='<f8', sep="")

def writef8(data, filename):
	ensuredirectories(filename)
	with open(filename, 'w+') as fid:
		data.tofile(fid) # little-endian

def ensuredirectories(filename):
	if not os.path.exists(os.path.dirname(filename)):
		try:
			os.makedirs(os.path.dirname(filename))
		except OSError as exc: # Guard against race condition
			if exc.errno != errno.EEXIST:
				raise
