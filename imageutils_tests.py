import lib.czifile as cz
import lib.ioutils as ioutils
import numpy
import pylab as pl
import imageutils
import plotutils
import matplotlib.pyplot as plt
import math

# TEST_FOLDER = 'output/mchtua5c1rectangle/'
# TEST_CASE = 'mchtua5c1rectangle'
# TEST_FOLDER = 'output/mchtua5c2triangle/'
# TEST_CASE = 'mchtua5c2triangle'

class Imageutils_tests(object):

    def __init__(self, dataname, dirname = ''):
        self.__dataname = dataname
        if not dirname:
            self.__dirname = 'output/' + dataname + '/'
        else:
            self.__dirname = 'output/' + dirname + '/'
        self.__tol = 0.001

    def validateZProjectionUsingMIP(self, indata, display = False):
        testfile = self.__dirname + 'MAX_' + self.__dataname + '.czi - C=0.raw'
        try:
            # Fiji raw format is uint16 big-endian
            testdata = ioutils.loadu2(testfile).reshape(indata.shape)
            # compare images - computes absolute diff
            delta = imageutils.computeError(indata, testdata, 'percentError')
            # and sum error
            error = numpy.sum(delta)/delta.size
            if(error==0):
                return 'z projection test passed.' + str(error)
            else:
                return 'z projection test failed: ' + str(error)
        except Exception, e:
            return 'z projection test failed.' + str(e)

    def validatePartialSpatialDerivativeX(self, indata, display=False):
        testfile = self.__dirname + 'Gradient-X-1.raw';
        try:
            indata = imageutils.rescale(indata,0.0,1.0)
            # Fiji raw format is real 32bits big-endian
            testdata = ioutils.loadf4(testfile).reshape(indata.shape)
            # compare images - computes absolute diff
            delta = imageutils.computeError(indata, testdata, 'percentError')
            # and sum error
            error = numpy.sum(delta)/delta.size
            if(display):
                pl.matshow(delta)
                pl.clim(0,100)
                pl.colorbar()
                pl.title('Error dx')
            if(error<self.__tol):
                return 'derivative X test passed.' + str(error)
            else:
                return 'derivative X test failed: ' + str(error)
        except Exception, e:
            return 'derivative X test failed.' + str(e)

    def validatePartialSpatialDerivativeY(self, indata, display=False):
        testfile = self.__dirname + 'Gradient-Y-1.raw';
        try:
            indata = imageutils.rescale(indata,0.0,1.0)
            # Fiji raw format is real 32bits big-endian
            testdata = ioutils.loadf4(testfile).reshape(indata.shape)
            # compare images - computes absolute diff
            delta = imageutils.computeError(indata, testdata, 'percentError')
            # and sum error
            error = numpy.sum(delta)/delta.size
            if(display):
                pl.matshow(delta)
                pl.clim(0,100)
                pl.colorbar()
                pl.title('Error dy')
            if(error<self.__tol):
                return 'derivative Y test passed.' + str(error)
            else:
                return 'derivative Y test failed: ' + str(error)
        except Exception, e:
            return 'derivative Y test failed.' + str(e)

    def validateOrientation(self, indata, display=False):
        testfile = self.__dirname + 'Orientation-1.raw'
        try:
            testdata = ioutils.loadf4(testfile).reshape(indata.shape)
            # compare images
            delta = imageutils.computeError(indata, testdata, 'percentError')
            # and sum error
            error = numpy.sum(delta)/delta.size
            if(display):
                plotutils.plotImageResults(indata, testdata, delta, 'orientation')
            if(error<=self.__tol * 100):
                return 'orientation test passed.' + str(error)
            else:
                return 'orientation test failed: ' + str(error)
        except Exception, e:
            return 'orientation test failed.' + str(e)

    def validateEnergy(self, indata, display=False):
        testfile = self.__dirname + 'Energy-1.raw'
        indata = imageutils.rescale(indata, 0.0, 1.0)
        try:
            testdata = ioutils.loadf4(testfile).reshape(indata.shape)
            # compare images
            delta = imageutils.computeError(indata, testdata, 'percentError')
            # and sum error
            error = numpy.sum(delta)/delta.size
            if(display):
                plotutils.plotImageResults(indata, testdata, delta, 'energy')
            if(error < self.__tol):
                return 'energy test passed.' + str(error)
            else:
                return 'energy test failed: ' + str(error)
        except Exception, e:
            return 'energy test failed.' + str(e)

    def validateCoherency(self, indata, display=False):
        testfile = self.__dirname + 'Coherency-1.raw'
        try:
            testdata = ioutils.loadf4(testfile).reshape(indata.shape)
            # compare images - computes absolute diff
            delta = imageutils.computeError(indata, testdata, 'percentError')
            # and sum error
            error = numpy.sum(delta)/delta.size
            if(display):
                plotutils.plotImageResults(indata, testdata, delta, 'coherency')
            if(error<self.__tol):
                return 'coherency test passed.' + str(error)
            else:
                return 'coherency test failed: ' + str(error)
        except Exception, e:
            return 'coherency test failed.' + str(e)
