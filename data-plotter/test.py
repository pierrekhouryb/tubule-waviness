import math
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import os
cwd = os.getcwd()

filename = os.path.join(cwd,"data-plotter","0p8mannitol_cell1.TXT")
# filename = "0p8mannitol_cell1.TXT"

def standardize(xp, yp):
    rxp = np.round(xp)
    new_x = []
    new_y = []
    counts = []
    for (i, xi) in enumerate(rxp):
        if(not math.isnan(xi)):
            if(xi in new_x):
                new_y[-1] = new_y[-1]+yp[i]
                counts[-1] = counts[-1] + 1
            else:
                new_x.append(rxp[i])
                new_y.append(yp[i])
                counts.append(1)

    new_y = np.divide(new_y, counts)
    return (new_x, new_y)

def linear_sampling(xp, yp, x):
    new_y = np.zeros(len(x))
    index = 0
    if(x[0]<xp[0]):
        new_y[0:int(xp[0])] = 0
        index = int(xp[0])
    else:
        new_y[0:int(x[0])] = 0
        index = int(x[0])

    for (i, xi) in enumerate(xp):
        if(i>0):
            d = int(xp[i]-xp[i-1])
            new_y[index:index+d]=yp[i]
            index = index + d

    if(x[-1]>xp[-1]):
        d = x[-1]-xp[-1]
        new_y[index:index+d]=yp[-1]
    else:
        d = xp[-1]-x[-1]
        new_y[index:index+d]=yp[-1]

    return new_y

df = pd.read_table(filename,"\t")

# extract intensity data
t = np.squeeze(df.as_matrix(columns=["time s"]))
I = np.squeeze(df.as_matrix(columns=["I/Io"]))
# extract force data
tp = np.squeeze(df.as_matrix(columns=["time sensor"]))
Fp = np.squeeze(df.as_matrix(columns=["F nN"]))

# first, we change the time sensor axis resolution to second
# we average sensor values if needed
(ts, Fs) = standardize(tp, Fp)
# then we perform a simple resampling of the sensor values based on the time axis.
F = linear_sampling(ts, Fs, t)

f, ax = plt.subplots(2,1)
# plot intensity versus time
ax[0].plot(t, I, 'k--',label='intensity')
ax[0].set_xlabel('time (s)')
ax[0].set_ylabel('intensity')
axF = ax[0].twinx()
# plot force versus time
axF.plot(tp, Fp, 'b+',label='force (nN)')
axF.plot(t, F, 'r',label='f resampled (nN)')
axF.set_ylabel('force (nN)', color='r')
axF.tick_params('y', colors='r')
# plot intensity versus force
ax[1].plot(F, I, '+')
ax[1].set_xlabel('force (nN)')
ax[1].set_ylabel('intensity')
plt.show()
