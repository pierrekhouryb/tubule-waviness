import matplotlib.pyplot as plt
import numpy as numpy
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib.colors import LogNorm
from matplotlib.ticker import MultipleLocator

def plotImageResults(computed, expected, delta, kind):
    if(kind == 'energy' or kind == 'coherency'):
        CLIM = (0.0, 1.0)
    else:
        CLIM = (-1.4, 1.4)

    fig, (ax1, ax2, ax3) = plt.subplots(1,3)
    fig.suptitle(kind, fontsize=20)

    ax1.set_title('computed')
    im1 = ax1.imshow(computed, clim=CLIM, aspect='auto')
    divider1 = make_axes_locatable(ax1)
    cax1 = divider1.append_axes("right", size="20%", pad=0.05)
    cbar1 = plt.colorbar(im1, cax=cax1)
    ax1.xaxis.set_visible(False)

    ax2.set_title('expected')
    im2 = ax2.imshow(expected, clim=CLIM, aspect='auto')
    divider2 = make_axes_locatable(ax2)
    cax2 = divider2.append_axes("right", size="20%", pad=0.05)
    cbar2 = plt.colorbar(im2, cax=cax2)
    ax2.xaxis.set_visible(False)

    ax3.set_title('error')
    im3 = ax3.imshow(delta, clim=(0.0, 100.0), aspect='auto')
    divider3 = make_axes_locatable(ax3)
    cax3 = divider3.append_axes("right", size="20%", pad=0.05)
    cbar3 = plt.colorbar(im3, cax=cax3)
    ax3.xaxis.set_visible(False)

    plt.subplots_adjust(top=0.85)

def plotHisto(image, mask=None):
    if(mask is None):
        mask = numpy.zeros(image.shape) + 1
    fig, ax = plt.subplots()
    img1d = image.ravel()
    h, binEdges = numpy.histogram(img1d, bins=256, range=(numpy.min(img1d), numpy.max(img1d)), weights=mask.ravel())
    bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
    ax.plot(bincenters,h,'-')

def plotImage(image, title=''):
    fig, ax = plt.subplots()
    fig.suptitle(title, fontsize=20)

    im = ax.imshow(image, aspect='auto')
    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="20%", pad=0.05)
    cbar = plt.colorbar(im, cax=cax)
    ax.xaxis.set_visible(False)
    plt.subplots_adjust(top=0.85)

# def plotAllAnalysis(input, orientation, coherency, energy):
#     fig, (ax1,ax2,ax3,ax4) = plt.subplots(2,2)
